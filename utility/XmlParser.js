function XmlParser() {}

XmlParser.prototype.parse = function parse(jsxml) {
  let msg;
  if (!Object.prototype.hasOwnProperty.call(jsxml, 'serviceResponse')) {
    msg = 'An unknown response was received from CAS server: "'
      + `${JSON.stringify(jsxml)}"`;
    return Promise.reject(jsxml);
  }

  const body = jsxml.serviceResponse;
  if (Object.prototype.hasOwnProperty.call(body, 'authenticationFailure')) {
    msg = 'Recieved bad validation from CAS server: "'
      + `${JSON.stringify(body.authenticationFailure)}"`;
    return Promise.reject(new Error(msg));
  }
  if (Object.prototype.hasOwnProperty.call(body, 'authenticationSuccess')) {
    return Promise.resolve(body.authenticationSuccess);
  }
};

module.exports = XmlParser;
