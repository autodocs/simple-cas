const xml2js = require('xml2js');
const XmlParser = require('./XmlParser');


const xmlParser = new XmlParser();

/**
 * Processes CAS protocol 3.0 result XML.
 *
 * @private
 * @param {string} xml The response to process.
 * @returns {Promise}
 */
function validate(xml) {
  return new Promise((resolve, reject) => {
    xml2js.parseString(
      xml,
      {
        explicitArray: false,
        tagNameProcessors: [name => (name.replace('cas:', ''))],
      },
      (err, jsxml) => {
        if (err) {
          return reject(err);
        }
        xmlParser.parse(jsxml)
          .then((result) => {
            if (result.attributes && result.attributes.memberOf
              && Array.isArray(result.attributes.memberOf) === false) {
              result.attributes.memberOf = [result.attributes.memberOf];
            }
            resolve(result);
          })
          .catch(err2 => reject(err2));
      },
    );
  });
}

module.exports = validate;
