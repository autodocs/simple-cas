/**
 * Processes CAS protocol 1.0 result
 *
 * @private
 * @param {string} response The response to process.
 * @returns {Promise}
 */
function validate(response) {
  function promise(resolve, reject) {
    let msg;
    const parts = response.toLowerCase().split('\n');
    if (parts.length < 2) {
      msg = `Received invalid CAS 1.0 validation response: ${response}`;
      return reject(new Error(msg));
    }
    if (parts[0] === 'yes') {
      return resolve(true);
    } if (parts[0] === 'no') {
      msg = 'Received bad validation from CAS 1.0 server';
      return reject(new Error(msg));
    }
  }

  return new Promise(promise);
}

module.exports = validate;
