/**
 *
 * @fileOverview This file exports the <i>validateServiceTicket</i> module that allows
 * authenticate module to validate ticket it got back after user signed in.
 */

const request = require('request');

/**
 * validate ticket from CAS login
 * @param servicePackage this package include full service url and full cas validate url and ticket
 * @return {Promise<any>}
 */
function validateServiceTicket(servicePackage) {
  const self = this;
  function promise(resolve, reject) {
    const reqOptions = {
      url: servicePackage.fullValidateUrl,
      qs: {
        ticket: servicePackage.ticket,
        service: servicePackage.fullServiceUrl,
      },
      strictSSL: self.strictSSL,
    };

    request(reqOptions, (error, response, body) => {
      if (error) {
        return reject(error);
      }

      if (response.statusCode !== 200) {
        return reject(new Error(
          `CAS server returned status: ${response.statusCode}`,
        ));
      }
      // parse xml result
      servicePackage.validate(body).then((msg) => {
        resolve(msg);
      }).catch((err) => {
        reject(err);
      });
    });
  }
  return new Promise(promise);
}

module.exports = validateServiceTicket;
