const _ = require('lodash');
const url = require('url');
const protocol2 = require('./protocol2');

const defaults = {
  protocol: 'https',
  host: undefined,
  port: 443,
  service: undefined,
  protocolVersion: 2.0, // default to 2.0
  validateTicketProtocol: protocol2, // default to 2.0 CAS ticket parsing
  paths: {
    serviceValidate: '/cas/serviceValidate', // default to 2.0 route
    login: '/cas/login',
    logout: '/cas/logout',
  },
};

module.exports = (options) => {
  if (!options) return _.cloneDeep(defaults);
  // ensure that service url is a valid url
  const data = _.extend(defaults, options);
  if (options.service) options.service = url.format(data.service);
  return _.extend(defaults, options);
};
