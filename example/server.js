const express = require('express');
const url = require('url');
const session = require('express-session');
const CAS = require('simple-cas');

CAS.configure({ host: 'your.cas.domain.host', service: 'http://localhost:3000/' });
const app = express();

// Use cookie sessions for simplicity, you can use something else
app.use(session({
  secret: 'supersecuretext',
  resave: false,
  saveUninitialized: true,
}));

app.get('/', (req, res) => {
  if (req.session.cas && req.session.cas.user) {
    return res.send(`<p>You are logged in. Your username is ${req.session.cas.user}. <a href="/logout">Log Out</a></p>`);
  }
  return res.send('<p>You are not logged in. <a href="/login">Log in now.</a><p>');
});

app.get('/login', CAS.authenticate(CAS.configure(), 'login'), (req, res) => {
  res.redirect('/');
});

app.get('/logout', (req, res) => {
  if (!req.session) {
    return res.redirect('/');
  }
  // Forget our own login session
  if (req.session.destroy) {
    req.session.destroy();
  } else {
    // Cookie-based sessions have no destroy()
    req.session = null;
  }
  // Send the user to the official campus-wide logout URL
  const options = CAS.configure();
  // set logout url
  options.pathname = options.paths.logout;
  // set return url after logout
  options.query = options.query || {};
  options.query.service = options.service;
  return res.redirect(url.format(options));
});

app.listen(3000);
