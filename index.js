exports.authenticate = require('./utility/authenticate');
exports.configure = require('./utility/configure');
exports.validateService = require('./utility/validateServiceTicket');
